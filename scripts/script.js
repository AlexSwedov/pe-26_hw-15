/* Теоритичні питання:
1. В чому відмінність між setInterval та setTimeout?

	setInterval виконує задані функції з заданою періодичністю, setTimeout виконує задані функції з заданою затримкою.

2. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?

	для припинення виконання функції запланованих з використанням setTimeout та setInterval використовується clearInterval() з id потрібного setTimeout чи setInterval.

Практичне завдання 1: 

-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати, що операція виконана успішно.
*/

const btn = document.querySelector('button');
const blockTest = document.querySelector('.test__result');

btn.addEventListener('click', (event) => {
	setTimeout(() => {
		blockTest.innerText = 'Тест завершено успішно';
	}, 3000);
});

/*
Практичне завдання 2: 

Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. 
Після досягнення 1 змініть текст на "Зворотній відлік завершено".
*/

const loading = document.querySelector('.counting__result');

document.addEventListener('DOMContentLoaded', () => {
	loading.innerText = '10';
	let num = 10;
	const timer = setInterval(() => {
		num--;
		loading.innerText = `${num}`;
		if (num === 0) {
			loading.innerText = 'Зворотній відлік завершено';
			clearInterval(timer);
		}
	}, 1000);
})
